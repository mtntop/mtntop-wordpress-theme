<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-07-15
 * Time: 18:23
 */

get_header();

if (have_posts()) :
    $post_type = get_post_type();
    $is_active_sidebar = is_active_sidebar( $post_type );
    ?>

    <div class="container wp-bootstrap-blocks-parent">

        <h1 class="mb-5"><?php echo get_the_archive_title(); ?></h1>

        <div class="row">
            <?php if ($is_active_sidebar) : ?>
                <div class="col-md-3">
                    <?php dynamic_sidebar( $post_type ); ?>
                </div>
            <?php endif; ?>

            <div class="<?php echo $is_active_sidebar ? 'col-md-9' : 'col-md-12'; ?>">
                <div class="posts-loop posts-loop_<?=$post_type?>">
                    <?php
                    while (have_posts()) :
                        the_post();

                        get_template_part('views/loop/post', $post_type);
                    endwhile;
                    ?>
                </div>

                <?php tpl('views/parts/shared/pagination'); ?>
            </div>
        </div>

    </div>

<?php
endif;

get_footer();