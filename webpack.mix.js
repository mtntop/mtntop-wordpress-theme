let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

mix.sass('src/scss/frontend.scss', 'assets/css/frontend.min.css')
    .sass('src/scss/admin.scss', 'assets/css/admin.min.css')
    .sass('src/scss/page-editor.scss', 'assets/css/page-editor.min.css')
    .js('src/js/frontend.js', 'assets/js/frontend.min.js');
