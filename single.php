<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 */

get_header();

if (have_posts()) :
    $post_type = get_post_type();
    $is_active_sidebar = is_active_sidebar( $post_type );
    ?>

    <div class="container wp-bootstrap-blocks-parent">

    <div class="row">
        <?php if ($is_active_sidebar) : ?>
            <div class="col-md-3">
                <?php dynamic_sidebar( $post_type ); ?>
            </div>
        <?php endif; ?>

        <div class="<?php echo $is_active_sidebar ? 'col-md-9' : 'col-md-12'; ?>">
            <?php
            while (have_posts()) :
                the_post();

            get_template_part('views/singular/content', $post_type);

            endwhile;
            ?>
        </div>

    </div>

<?php
endif;

get_footer();