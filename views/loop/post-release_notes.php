<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-07-15
 * Time: 18:27
 */
?>

<section class="release-notes--item mb-5">
    <h2 class="card-title"><?php the_title(); ?></h2>
    <div class="card-text"><?php the_content(); ?></div>
</section>

<hr class="mb-5">
