<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-07-17
 * Time: 20:31
 */

?>

<div class="container wp-bootstrap-blocks-parent">

    <h1><?php _e('404 Page not Found', CoopTheme\PREFIX); ?></h1>
    <p><?php echo sprintf(
            __('The page you visited is not exist. Return to %sHome page%s.', CoopTheme\PREFIX),
            '<a href="' . get_bloginfo('url') . '">"',
            '</a>'
        ); ?></p>

</div>

<?php