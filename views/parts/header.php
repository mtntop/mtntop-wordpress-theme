<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 18:04
 */

$header_transparent = get_field('header_transparent');
$header_classes = $header_transparent ? 'container header_transparent' : '';
$nav_classes = $header_transparent ? '' : 'bg-primary navbar-dark';
$header_logo_size = get_field('header-logo-size');
?>

    <header class="<?=$header_classes?>">
        <nav class="navbar navbar-expand-lg <?=$nav_classes?>">

            <div class="container">
                <?php

                tpl('views/parts/navbar/brand', ['size' => $header_logo_size]);

                tpl('views/parts/navbar/toggler', ['id' => 1]);

                wp_nav_menu( array(
                    'theme_location'  => 'primary',
                    'depth'	          => 2,
                    'container'       => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id'    => 'bs-example-navbar-collapse-1',
                    'menu_class'      => 'navbar-nav mr-auto',
                    'fallback_cb'     => 'CoopTheme\WP_Bootstrap_Navwalker::fallback',
                    'walker'          => new CoopTheme\WP_Bootstrap_Navwalker(),
                ) );

                ?>
            </div>
        </nav>
    </header>