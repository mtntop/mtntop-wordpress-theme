<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 12:16
 */

$title = isset($title) ? $title : '';
$action_url = isset($post_type) ? get_post_type_archive_link($post_type) : null;
$s = isset($_GET['s']) ? $_GET['s'] : '';
?>

<form role="search" method="get" id="searchform" class="searchform" action="<?=$action_url?>">
    <div class="form-group">
        <?php if ($title ) : ?>
            <label class="screen-reader-text widgettitle" for="s"><?=$title?></label>
        <?php endif; ?>

        <div class="input-wrapper type type_search">
            <inputы
                    class="form-control"
                    type="text"
                    value="<?=$s?>"
                    placeholder="<?php _e('Type a keyword here...', \CoopTheme\PREFIX); ?>"
                    name="s"
                    id="s">
        </div>
    </div>
</form>
