<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 12:16
 */

$wp_query = array_key_exists('wp_query', $GLOBALS) ? $GLOBALS['wp_query'] : '';

if ($wp_query) :
    $pages = '';

    $max   = $wp_query->max_num_pages;

    if ( ! $current = get_query_var('paged') )
        $current = 1;

    $a['base']    = str_replace(999999999, '%#%', get_pagenum_link(999999999));
    $a['total']   = $max;
    $a['current'] = $current;

    $total = 0;

    $a['mid_size']  = 1;
    $a['end_size']  = 1;
    $a['prev_text'] = '&laquo;';
    $a['next_text'] = '&raquo;';
    $a['type'] = 'array';

    if ( $max > 1 )
        echo '<nav class="pagination-wrapper"><ul class="pagination">';

    $links = paginate_links($a);

    if (is_array($links)) :
        foreach ($links as $link) :
            $link = str_replace('page-numbers', 'page-link page-numbers', $link);
            $link = str_replace('dots', 'dots disabled', $link);
            $link = str_replace('current', 'current active', $link);

            echo '<li class="page-item">' . str_replace('page-numbers', 'page-link page-numbers', $link) . '</li>';
        endforeach;
    endif;


    if ($max > 1)
        echo '</ul></nav>';
endif;
?>
