<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 12:24
 */

$socials = get_field('social-networks', 'options');

if ($socials) :
    ?>

    <div class="social btn-group btn-group-sm" role="group" aria-label="<?php _e('Social media', CoopTheme\PREFIX); ?>">
        <?php foreach ($socials as $social) : ?>
            <?php if (array_key_exists('acf_fc_layout', $social)) : ?>
                <a href="<?=$social['url']?>" target="_blank" class="btn  btn-light socicon-<?=$social['acf_fc_layout']?>"></a>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>

    <?php

endif;
