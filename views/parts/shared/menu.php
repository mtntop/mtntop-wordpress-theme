<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 12:36
 */

if (isset($theme_location)) :
    $locations = get_nav_menu_locations();

    if (array_key_exists($theme_location, $locations)) :
        $menu = wp_get_nav_menu_object( $locations[$theme_location] );
        ?>

        <h2 class="menu-name"><?php echo $menu->name ?></h2>

        <?php
        wp_nav_menu( [
            'theme_location'  => isset($theme_location) ? $theme_location : 'primary',
            'depth'	          => isset($depth) ? $depth : 1,
            'container'       => 'div',
            'menu_class'      => isset($menu_class) ? $menu_class : 'navbar-nav mr-auto',
            'fallback_cb'     => 'CoopTheme\WP_Bootstrap_Navwalker::fallback',
            'walker'          => new CoopTheme\WP_Bootstrap_Navwalker(),
        ] );
    endif;
endif;
