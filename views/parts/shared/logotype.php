<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 12:16
 */


$logotype = get_field('logotype', 'options');
$header_logo_size = isset($header_logo_size) ? $header_logo_size : '140px';

if ($logotype) :
    echo wp_get_attachment_image( $logotype, $header_logo_size );
else :
    echo '<h3 class="mb-0">' . get_bloginfo('name') . '</h3>';
endif;
