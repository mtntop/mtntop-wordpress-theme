<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 18:59
 */

$websites = get_field('hoa-websites', 'options');

if ($websites) :
    ?>

    <div class="hoa-websites container">
        <div class="row">

            <?php
            foreach ($websites as $website) :
                $url = array_key_exists('url', $website) && $website['url'] ? $website['url'] : ':javascript';
                $image = array_key_exists('cover_image', $website) && $website['cover_image']
                    ? wp_get_attachment_image( $website['cover_image'] , '350px' )
                    : '';

                if ($image) :
                    ?>

                    <div class="col-sm-4 mb-3">

                        <a href="<?=$url;?>" target="_blank">
                            <?=$image?>
                        </a>

                    </div>

                    <?php
                endif;
            endforeach;
            ?>

        </div>
    </div>

    <?php
endif;