<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 18:59
 */

$testimonials = get_field('testimonials');

if ($testimonials) :
    ?>

    <div class="testimonials">
        <div class="testimonials__box">

                <div class="testimonials--list">
                    <?php foreach ($testimonials as $testimonial) : ?>
                        <?php $job_title = get_field('job_title', $testimonial->ID); ?>

                        <div class="testimonials--list__item">

                            <div class="testimonial bg-dark">

                                <?php if (has_post_thumbnail($testimonial->ID)) : ?>
                                    <div class="testimonial--thumbnail">
                                        <?php echo get_the_post_thumbnail($testimonial->ID, '120px'); ?>
                                    </div>
                                <?php endif; ?>

                                <h3 class="testimonial--title tt-uppercase mb-0"><?php echo $testimonial->post_title; ?></h3>

                                <?php echo $job_title ? "<div class='testimonial--job-title mb-0 mb-2'>$job_title</div>" : '' ?>

                                <blockquote class="testimonial--blockquote mb-0 mt-3">
                                    <?php echo $testimonial->post_excerpt; ?>
                                </blockquote>
                            </div>

                        </div>

                    <?php endforeach; ?>
                </div>
                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

        </div>
    </div>

    <?php
endif;