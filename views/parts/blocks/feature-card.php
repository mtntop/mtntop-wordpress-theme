<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 18:59
 */

$style = get_field('style');
$bg    = $style === 'style-2' ? 'bg-white' : 'bg-grey';
$bs    = $style === 'style-2' ? 'bs-normal' : 'bs-none';
$icon  = get_field('icon');
$headline  = get_field('headline');
$description  = get_field('description');
$reference  = get_field('reference');
?>

<div class="feature-card <?=$bg;?> <?=$bs;?> <?=$style;?>">
    <div class="feature-card__box">

        <?php if ($icon) : ?>
            <?php if (is_admin()) : ?>
                <img src="<?php echo $icon; ?>"
                     alt="<?php echo $headline . ' | ' . get_bloginfo('name'); ?>" />
            <?php else : ?>
                <img src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                     data-aload="<?php echo $icon; ?>"
                     alt="<?php echo $headline . ' | ' . get_bloginfo('name'); ?>" />

                <noscript>
                    <img src="<?php echo $icon; ?>"
                         alt="<?php echo $headline . ' | ' . get_bloginfo('name'); ?>" />
                </noscript>
            <?php endif; ?>
        <?php endif; ?>

        <h3 class="h6"><?php echo $headline; ?></h3>

        <p><?php echo $description; ?></p>

        <?php if ($reference['label']) : ?>
            <?php if ($style === 'style-2') : ?>
                <a href="<?=$reference['page'];?>" class="btn btn-primary"><?=$reference['label']?></a>
            <?php else: ?>
                <a href="<?=$reference['page'];?>" class="btn btn-link"><?=$reference['label']?></a>
            <?php endif; ?>
        <?php endif; ?>
        
    </div>
</div>
