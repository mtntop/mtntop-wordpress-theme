<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 18:04
 */

$footer_white = get_field('footer_white');
$footer_classes = $footer_white ? 'bg-white' : 'bg-primary';
$nav_classes = $footer_white ? '' : 'navbar-dark';
$footer = get_field('footer', 'options');
$footer_layout = get_field('footer-layout', 'options') ? get_field('footer-layout', 'options') : 'content-1-col';
?>

<footer class="site-footer <?=$footer_classes?>">
    <nav class="navbar <?=$nav_classes?>">
        <div class="container">
            <div class="row w-full align-items-center">
                <?php tpl("views/parts/footer/$footer_layout", ['footer' => $footer]); ?>
            </div>
        </div>
    </nav>

    <div class="navbar navbar-dark">
        <div class="container">
            <p><?php echo sprintf( __('© %s All Rights Reserved'), date('Y') )?></p>
        </div>
    </div>
</footer>

