<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-07-15
 * Time: 17:11
 */
?>

<div class="col-md-4 mb-sm-20">
    <?php echo $footer ? $footer : get_tpl('views/parts/navbar/brand', ['size' => 'large']); ?>

    <?php tpl('views/parts/shared/social'); ?>
</div>

<div class="col-md-8">
    <div class="row w-full">
        <div class="col-sm-4">
            <?php
            tpl('views/parts/shared/menu', [
                'theme_location' => 'footer-col-1',
                'menu_class' => 'menu-vertical navbar-nav mr-auto'
            ]);
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            tpl('views/parts/shared/menu', [
                'theme_location' => 'footer-col-2',
                'menu_class' => 'menu-vertical navbar-nav mr-auto'
            ]);
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            tpl('views/parts/shared/menu', [
                'theme_location' => 'footer-col-3',
                'menu_class' => 'menu-vertical navbar-nav mr-auto'
            ]);
            ?>
        </div>
    </div>
</div>