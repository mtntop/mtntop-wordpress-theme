<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-07-15
 * Time: 17:11
 */
?>

<div class="col-md-6 mb-sm-20">
    <?php echo $footer ? $footer : get_tpl('views/parts/navbar/brand', ['size' => 'large']); ?>

    <?php tpl('views/parts/shared/social'); ?>
</div>

<div class="col-md-6">
    <?php
    tpl('views/parts/shared/menu', [
        'theme_location' => 'footer',
        'menu_class' => 'navbar-nav mr-auto'
    ]);
    ?>
</div>

