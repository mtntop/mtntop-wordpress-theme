<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 12:19
 */

$id = isset($id) ? $id : 1;
?>

<button
    class="navbar-toggler"
    type="button"
    data-toggle="collapse"
    data-target="#bs-example-navbar-collapse-<?=$id?>"
    aria-controls="bs-example-navbar-collapse-<?=$id?>"
    aria-expanded="false"
    aria-label="<?php _e('Toggle navigation', CoopTheme\PREFIX); ?>">

    <span class="navbar-toggler-icon"></span>
</button>
