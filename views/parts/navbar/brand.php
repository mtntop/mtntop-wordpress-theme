<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-30
 * Time: 12:18
 */

$header_logo_size = isset($header_logo_size) ? $header_logo_size : '140px';

if (!is_front_page()) : ?>
    <a
        class="navbar-brand navbar-brand_<?=$header_logo_size?>"
        href="<?php echo get_bloginfo('url')?>"
        title="<?php _e('Back to Home page', CoopTheme\PREFIX); ?>">
        <?php tpl('views/parts/shared/logotype', ['size' => $header_logo_size]); ?>
    </a>
<?php else : ?>
    <div class="navbar-brand navbar-brand_<?=$header_logo_size?>">
        <?php tpl('views/parts/shared/logotype'); ?>
    </div>
<?php endif; ?>