'use strict';

import {featuredCard} from "./modules/featured-card";
import {aload} from "./modules/aload";

!(function($){

    $(function(){

        $('.testimonials--list').slick({
            slidesToShow: 2,
            arrows: false,
            dots: true,
            infinite: true,
            focusOnSelect: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });

        featuredCard();

        aload();

        $(document).on('scroll', () => aload());

    });

})(jQuery);