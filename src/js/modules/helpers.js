export function getScrolledBottomCorner() {
    return jQuery(window).scrollTop() + jQuery(window).height();
}

export function getOffset() {
    const nodeRect = this.getBoundingClientRect();
    const bodyRect = document.body.getBoundingClientRect();

    return nodeRect.top - bodyRect.top;
}