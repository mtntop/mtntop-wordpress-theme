export function featuredCard($element) {
    $element = $element ? $element : jQuery('.feature-card');

    $element.closest('.row').each(function(){
        const $row = jQuery(this);
        let theHighest = 0;

        $row.find('.feature-card').each(function(){
            const height = jQuery(this).height();

            if (height > theHighest) {
                theHighest = height;
            }
        }).each(function() {
            jQuery(this).height(theHighest);
        });
    });
}