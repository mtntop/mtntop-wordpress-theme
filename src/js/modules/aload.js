import {getScrolledBottomCorner, getOffset} from "./helpers";
import {featuredCard} from "./featured-card";

/**
 *
 * @param nodes
 * @returns {*}
 */
export function aload(nodes) {

    function removeAttr() {

        const node = this;

        node[ node.tagName !== 'LINK' ? 'src' : 'href' ] = node.getAttribute(attribute);

        if (node.classList.contains('attachment-svg')) {

            const img = new Image();

            // Set divider height
            img.onload = () => this.removeAttribute(attribute);

            // assign url to new image
            img.src = node.getAttribute(attribute);

        } else if (jQuery(node).closest('.slick-slider').length) {

            const img = new Image();

            // Set divider height
            img.onload = () => {
                this.removeAttribute(attribute);
                jQuery(node).closest('.slick-slider').slick('setOption', 'height', null, true); // ?
            };

            // assign url to new image
            img.src = node.getAttribute(attribute);

        } else if (jQuery(node).closest('.feature-card').length) {

            const img = new Image();

            // Set divider height
            img.onload = () => {
                this.removeAttribute(attribute);
                featuredCard(jQuery(node));
            };

            // assign url to new image
            img.src = node.getAttribute(attribute);

        } else {
            this.removeAttribute(attribute);
        }


        if (node.getAttribute(attributeSet)) {
            node.setAttribute('srcset', this.getAttribute(attributeSet));
            node.removeAttribute(attributeSet);
        }
    }

    const attribute = 'data-aload';
    const attributeSet = 'data-aload-srcset';

    nodes = nodes || window.document.querySelectorAll('[' + attribute + ']');

    if (nodes.length === undefined) {
        nodes = [nodes];
    }

    [].forEach.call(nodes, function (node) {

        if (node.tagName === 'IMG' || node.tagName === 'IFRAME') {
            const offset = getOffset.call(node);

            if (offset && (getScrolledBottomCorner() >= offset)) {
                removeAttr.call(node);
            }
        } else {
            removeAttr.call(node);
        }
    });

    return nodes;
}
