<?php
/**
 * Core file
 */
include_once get_template_directory() . '/autoload/core.php';

/**
 * @param       $file
 * @param array $vars
 * @return mixed
 */
function get_tpl($file, $vars = array()) {
    return class_exists('CoopTheme\Utility') ? CoopTheme\Utility::get_tpl($file, $vars) : '';
}
/**
 * @param       $file
 * @param array $vars
 * @return mixed
 */
function tpl($file, $vars = array()) {
    echo get_tpl($file, $vars);
}
/**
 * @param array $data
 * @param       $exit
 */
function pr($data = array(), $exit = false) {
    if (class_exists('CoopTheme\Utility')) :
        CoopTheme\Utility::pr($data, $exit);
    else :
        print_r($data);

        if ($exit) exit;
    endif;
}