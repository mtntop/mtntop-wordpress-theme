<?php
/**
 * Post types
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class PostTypes {

    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fields constructor.
     */
    function __construct () {

        add_action( 'init', __CLASS__ . '::register_testimonials_post_type' );

        add_action( 'init', __CLASS__ . '::register_announcement_post_type' );

        add_action( 'init', __CLASS__ . '::register_release_notes_post_type' );

        add_action( 'init', __CLASS__ . '::register_knowledge_base_post_type' );

    }


    public static function register_knowledge_base_post_type() {
        $labels = array(
            'name'                => __('Knowledge Base', PREFIX),
            'singular_name'       => __('Article', PREFIX),
            'search_items'        => __('Search', PREFIX),
            'all_items'           => __('All Articles', PREFIX),
            'edit_item'           => __('Edit Article', PREFIX),
            'update_item'         => __('Update Article', PREFIX),
            'add_new_item'        => __('Add Article', PREFIX),
            'new_item_name'       => __('Title', PREFIX),
            'menu_name'           => __('Knowledge Base', PREFIX)
        );

        $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_icon'             => 'dashicons-book-alt',
            'query_var'             => true,
            'show_in_rest'          => true,
            'capability_type'       => 'post',
            'has_archive'           => true,
            'hierarchical'          => false,
            'rewrite' 				=> array('slug' => 'knowledge-base','with_front' => true, 'pages'=> true),
            'supports'              => array('title', 'author', 'editor')
        );

        register_post_type( 'knowledge-base', $args );

        register_taxonomy('base-category', array('knowledge-base'), array(
            'label'                 => '', // определяется параметром $labels->name
            'labels'                => array(
                'name'              => __('Categories', PREFIX),
                'singular_name'     => __('Category', PREFIX),
                'search_items'      => __('Search Category', PREFIX),
                'all_items'         => __('All Categories', PREFIX),
                'view_item '        => __('View Category', PREFIX),
                'parent_item'       => __('Parent Category', PREFIX),
                'parent_item_colon' => __('Parent Category:', PREFIX),
                'edit_item'         => __('Edit Category', PREFIX),
                'update_item'       => __('Update Category', PREFIX),
                'add_new_item'      => __('Add New Category', PREFIX),
                'new_item_name'     => __('New Category Name', PREFIX),
                'menu_name'         => __('Category', PREFIX),
            ),
            'public'                => true,
            'publicly_queryable'    => null,
            'show_in_nav_menus'     => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_tagcloud'         => true,
            'show_in_rest'          => null,
            'rest_base'             => null,
            'hierarchical'          => true,
            'rewrite'               => true,
            'capabilities'          => array(),
            'meta_box_cb'           => null,
            'show_admin_column'     => false,
            '_builtin'              => false,
            'show_in_quick_edit'    => null,
        ) );
    }


    public static function register_release_notes_post_type() {
        $labels = array(
            'name'                => __('Release Notes', PREFIX),
            'singular_name'       => __('Release Note', PREFIX),
            'search_items'        => __('Search', PREFIX),
            'all_items'           => __('All Release Notes', PREFIX),
            'edit_item'           => __('Edit Release Note', PREFIX),
            'update_item'         => __('Update Release Note', PREFIX),
            'add_new_item'        => __('Add Release Note', PREFIX),
            'new_item_name'       => __('Title', PREFIX),
            'menu_name'           => __('Release Notes', PREFIX)
        );

        $args = array(
            'labels'                => $labels,
            'public'                => true,
            'publicly_queryable'    => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_icon'             => 'dashicons-media-document',
            'query_var'             => true,
            'capability_type'       => 'post',
            'has_archive'           => true,
            'hierarchical'          => false,
            'show_in_rest'          => true,
            'rewrite' 				=> array('slug' => 'all-release-notes','with_front' => false, 'pages'=> true),
            'supports'              => array('title', 'author', 'page-attributes', 'thumbnail', 'editor')
        );

        register_post_type( 'release_notes', $args );
    }


    /**
     * Register post type
     */
    public static function register_testimonials_post_type() {


        /**
         * @link https://wp-kama.ru/function/register_post_type
         */
        register_post_type('testimonial', array(
            'label'  => null,
            'labels' => array(
                'name'               => __('Testimonials', PREFIX),
                'singular_name'      => __('Testimonial', PREFIX),
                'add_new'            => __('Add Testimonial', PREFIX),
                'add_new_item'       => __('Add new Testimonial', PREFIX),
                'edit_item'          => __('Edit Testimonial', PREFIX),
                'new_item'           => __('New Testimonial', PREFIX),
                'view_item'          => __('See Testimonial', PREFIX),
                'search_items'       => __('Search Testimonial', PREFIX),
                'not_found'          => __('Not Found', PREFIX),
                'not_found_in_trash' => __('Not Found in Trash', PREFIX),
                'parent_item_colon'  => '',
                'menu_name'          => __('Testimonials', PREFIX),
            ),
            'description'         => '',
            'public'              => true,
            'publicly_queryable'  => true,
            'exclude_from_search' => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => true,
            'show_in_rest'        => true,
            'rest_base'           => true,
            'menu_position'       => null,
            'menu_icon'           => 'dashicons-groups',
            'hierarchical'        => true,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail' ), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
            'has_archive'         => true,
            'rewrite'             => true,
            'query_var'           => true,
        ) );


    }

    /**
     * Register post type
     */
    public static function register_announcement_post_type() {


        /**
         * @link https://wp-kama.ru/function/register_post_type
         */
        register_post_type('announcement', array(
            'label'  => null,
            'labels' => array(
                'name'               => __('Announcements', PREFIX),
                'singular_name'      => __('Announcement', PREFIX),
                'add_new'            => __('Add Announcement', PREFIX),
                'add_new_item'       => __('Add new Announcement', PREFIX),
                'edit_item'          => __('Edit Announcement', PREFIX),
                'new_item'           => __('New Announcement', PREFIX),
                'view_item'          => __('See Announcement', PREFIX),
                'search_items'       => __('Search Announcements', PREFIX),
                'not_found'          => __('Not Found', PREFIX),
                'not_found_in_trash' => __('Not Found in Trash', PREFIX),
                'parent_item_colon'  => '',
                'menu_name'          => __('Announcements', PREFIX),
            ),
            'description'         => '',
            'public'              => true,
            'publicly_queryable'  => true,
            'exclude_from_search' => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => true,
            'show_in_rest'        => true,
            'rest_base'           => true,
            'menu_position'       => null,
            'menu_icon'           => 'dashicons-format-aside',
            'hierarchical'        => true,
            'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail' ), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
            'has_archive'         => true,
            'rewrite' 			  => array('slug' => 'announcements','with_front' => true, 'pages'=> true),
            'query_var'           => true,
        ) );


    }

}