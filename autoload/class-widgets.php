<?php
/**
 * Post types
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class Widgets {

    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fields constructor.
     */
    function __construct () {

        //autoload files from `/autoload`
        spl_autoload_register( __CLASS__ . '::autoload' );

        add_action( 'widgets_init', __CLASS__ . '::register_widgets' );

    }


    /**
     * Includes all files with "class-" prefix
     *
     * @since 1.0.0
     * @param $filename
     */
    public static function autoload($filename){

        $dir = get_template_directory() . '/autoload/widgets/class-*.php';
        $paths = glob($dir);

        if (defined('GLOB_BRACE')) {
            $paths = glob( '{' . $dir . '}', GLOB_BRACE );
        }

        if( is_array($paths) && count($paths) > 0 ){
            foreach( $paths as $file ) {
                if ( file_exists( $file ) ) {
                    include_once $file;
                }
            }
        }
    }

    public static function register_widgets() {

        register_widget( 'CoopTheme\ReleaseNotesArchivesWidget' );

        register_widget( 'CoopTheme\KnowledgeBaseSearchWidget' );

    }
}