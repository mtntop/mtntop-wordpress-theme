<?php
/**
 * This is a utility class, it contains useful methods
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @link https://github.com/liip/bootstrap-blocks-wordpress-plugin
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class WpBlocks {


    /**
     * @var null
     */
    protected static $instance = null;
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function instance() {
        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    /**
     * Fields constructor.
     */
    function __construct () {

        /**
         * The code below depends on the two plugins
         */
        if (defined( 'WP_BOOTSTRAP_BLOCKS_PLUGIN_FILE' ) && class_exists('\ACF')) :

            add_action('acf/init', __CLASS__ . '::register_blocks');

        endif;

    }


    public static function register_blocks() {
        // check function exists
        if ( function_exists('acf_register_block') ) {

            acf_register_block(array(
                'name'				=> 'feature-card',
                'title'				=> __('Feature Card', PREFIX),
                'description'		=> __('A card containing an icon, text, and a link.', PREFIX),
                'render_callback'	=> __CLASS__ . '::block_render_callback',
                'category'			=> 'formatting',
                'keywords'			=> array( 'card', 'feature' ),
            ));

            acf_register_block(array(
                'name'				=> 'testimonials',
                'title'				=> __('Testimonials', PREFIX),
                'description'		=> __('A carousel containing the testimonials.', PREFIX),
                'render_callback'	=> __CLASS__ . '::block_render_callback',
                'category'			=> 'formatting',
                'keywords'			=> array( 'testimonials', 'reviews', 'feedback' ),
            ));

            acf_register_block(array(
                'name'				=> 'hoa-websites',
                'title'				=> __('HOA Websites', PREFIX),
                'description'		=> __('A list of HOW websites', PREFIX),
                'render_callback'	=> __CLASS__ . '::block_render_callback',
                'category'			=> 'formatting',
                'keywords'			=> array( 'hoa', 'websites' ),
            ));

            acf_register_block(array(
                'name'				=> 'app-links',
                'title'				=> __('App Links', PREFIX),
                'description'		=> __('A list of images wrapped by links leading to mobile stores. The links are fetched from Downloads menu.', PREFIX),
                'render_callback'	=> __CLASS__ . '::block_render_callback',
                'category'			=> 'formatting',
                'keywords'			=> array( 'apple', 'play', 'store' ),
            ));
        }
    }

    /**
     * @param array $block
     */
    public static function block_render_callback($block = array()) {

        $slug = str_replace('acf/', '', $block['name']);

        tpl("views/parts/blocks/{$slug}");
    }


}