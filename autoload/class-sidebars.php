<?php
/**
 * Post types
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class Sidebars {

    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fields constructor.
     */
    function __construct () {

        add_action( 'widgets_init', __CLASS__ . '::register_release_notes_sidebar' );

        add_action( 'widgets_init', __CLASS__ . '::register_knowledge_base_sidebar' );

    }

    public static function register_release_notes_sidebar() {
        register_sidebar( array(
            'id'          => 'release_notes',
            'name'        => __( 'Release Notes Sidebar', PREFIX ),
            'description' => __( 'This sidebar is located at left column of the Release Notes archive pages.', PREFIX ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => "</div>",
        ) );
    }

    public static function register_knowledge_base_sidebar() {
        register_sidebar( array(
            'id'          => 'knowledge-base',
            'name'        => __( 'Knowledge Base Sidebar', PREFIX ),
            'description' => __( 'This sidebar is located at left column of the Knowledge Base pages.', PREFIX ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => "</div>\n",
        ) );
    }
}