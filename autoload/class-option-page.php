<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2/9/19
 * Time: 5:50 PM
 */
namespace CoopTheme;

class OptionsPage {
    /**
     * @var null
     */
    protected static $instance = null;
    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function instance() {
        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    /**
     * Fields constructor.
     */
    function __construct () {
        if ( class_exists( '\ACF' ) && function_exists( 'acf_add_options_page' ) ) :
            // Options page
            add_action('acf/init', __CLASS__ . '::acf_add_options_page');
        endif;
    }
    /**
     * Creating Options Page
     *
     * @link https://www.advancedcustomfields.com/resources/acf_add_options_page/
     */
    public static function acf_add_options_page() {
        /**
         * @var $option_page - can be used for creating a child options page
         */
        $option_page = acf_add_options_page(array(
            'page_title' 	=> __('Theme Options', PREFIX),
            'menu_title' 	=> __('Theme Options', PREFIX),
            'menu_slug' 	=> 'theme-settings',
            'capability' 	=> 'edit_posts',
            'redirect' 	=> true,
            'icon_url' => false,
        ));
        // add sub page
        acf_add_options_sub_page(array(
            'page_title' 	=> __('Shared Components', PREFIX),
            'menu_title' 	=> __('Shared Components', PREFIX),
            'parent_slug' => $option_page['menu_slug'],
            'menu_slug'   => 'theme-shared'
        ));

        if (get_current_blog_id() === 6) :
            acf_add_options_sub_page(array(
                'page_title' 	=> __('HOA websites', PREFIX),
                'menu_title' 	=> __('HOA websites', PREFIX),
                'parent_slug' => $option_page['menu_slug'],
                'menu_slug'   => 'hoa-websites'
            ));
        endif;
    }
}