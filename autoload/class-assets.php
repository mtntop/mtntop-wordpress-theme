<?php
/**
 * This is a utility class, it contains useful methods
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class Assets {


    /**
     * Enqueue files
     *
     * @since 1.0.0
     * @return void
     */
    public static function enqueue(){

        add_action('wp_enqueue_scripts', __CLASS__ . '::frontend', 9999);

        // add_action('admin_enqueue_scripts', __CLASS__ . '::admin', 9999);

        add_action('admin_enqueue_scripts', __CLASS__ . '::page_editor', 9999);

    }


    /**
     * Enqueue styles in admin side
     *
     * @since 1.0.0
     * @return void
     */
    public static function page_editor() {

        $screen = get_current_screen();

        if ( $screen->post_type === 'page' && $screen->base === 'post' ) :

            wp_enqueue_style(
                __NAMESPACE__ . '_fonts',
                THEME_URL . '/assets/fonts/proxima-nova/fonts.min.css',
                array(),
                THEME_VERSION
            );

            wp_enqueue_style(
                __NAMESPACE__ . '_page_editor',
                THEME_URL .'/assets/css/page-editor.min.css',
                array(),
                THEME_VERSION
            );

        endif;

    }


    /**
     * Enqueue styles in frontend
     *
     * @since 1.0.0
     * @return void
     */
    public static function frontend(){

        wp_enqueue_style(
            'socicons',
            'https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?u8vidh',
            array(),
            THEME_VERSION
        );

        wp_enqueue_style(
            'slick-slider',
            '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css',
            array(),
            THEME_VERSION
        );

        wp_enqueue_style(
            __NAMESPACE__ . '_frontend',
            THEME_URL .'/assets/css/frontend.min.css',
            array('socicons'),
            THEME_VERSION
        );

        wp_enqueue_script(
            'bootstrap',
            '//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js',
            array(),
            THEME_VERSION
        );

        wp_enqueue_script(
            'slick-slider',
            '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',
            ['jquery'],
            THEME_VERSION,
            true
        );

        wp_enqueue_style(
            __NAMESPACE__ . '_fonts',
            THEME_URL . '/assets/fonts/proxima-nova/fonts.min.css',
            array(),
            THEME_VERSION
        );

        wp_enqueue_script(
            __NAMESPACE__ . '_frontend',
            THEME_URL .'/assets/js/frontend.min.js',
            ['jquery', 'slick-slider'],
            THEME_VERSION,
            true
        );

    }

}