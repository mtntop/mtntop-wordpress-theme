<?php
/**
 * Post types
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class GravityForms {

    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fields constructor.
     */
    function __construct () {

        add_filter( 'gform_field_css_class', __CLASS__ .  '::gfield_custom_class', 10, 3 );

    }

    public static function gfield_custom_class( $classes, $field, $form ) {
        $classes .= ' form-group col-12';

        return $classes;
    }

}