<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:56
 */

namespace CoopTheme;

if ( ! class_exists('Core') ) :

    define(__NAMESPACE__ . '\PREFIX', 'coop-wp-theme');

    define(__NAMESPACE__ . '\THEME_NAME', 'Coop Theme');

    define(__NAMESPACE__ . '\THEME_URL', get_template_directory_uri());

    define(__NAMESPACE__ . '\THEME_VERSION', '1.24');

    class Core {

        protected static $instance = null;

        /**
         * Return an instance of this class.
         *
         * @since     1.0.0
         *
         * @return    object    A single instance of this class.
         */
        public static function instance() {

            // If the single instance hasn't been set, set it now.
            if ( null == self::$instance ) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        function __construct() {

            //autoload files from `/autoload`
            spl_autoload_register( __CLASS__ . '::autoload' );

            Assets::enqueue();

            // init post types
            PostTypes::instance();

            // init acf fields
            FieldGroups::instance();

            // option page
            OptionsPage::instance();

            // gf
            GravityForms::instance();

            // sidebars
            Sidebars::instance();

            // widgets
            Widgets::instance();

            // init wp blocks
            WpBlocks::instance();

            add_action('after_setup_theme', __CLASS__ . '::setup');

            add_action('init', __CLASS__ . '::run_on_init');

            add_filter('image_size_names_choose', __CLASS__ . '::add_custom_image_sizes');

            add_filter('upload_mimes', __CLASS__ . '::add_mime_types', 1, 1);

            add_filter('wp_get_attachment_image_attributes', __CLASS__ . '::replace_src_with_data_aload', 50);

            add_filter( 'get_the_archive_title', __CLASS__ . '::get_the_archive_title', 999 );

            add_filter( 'nav_menu_item_title', __CLASS__ . '::set_app_images_nav_menu_item_title', 10, 4 );

            add_filter( 'get_archives_link', __CLASS__ . '::filter_archives_link', 10, 4 );
        }


        /**
         * Includes all files with "class-" prefix
         *
         * @since 1.0.0
         * @param $filename
         */
        public static function autoload($filename){

            $dir = get_template_directory() . '/autoload/class-*.php';
            $paths = glob($dir);

            if (defined('GLOB_BRACE')) {
                $paths = glob( '{' . $dir . '}', GLOB_BRACE );
            }

            if( is_array($paths) && count($paths) > 0 ){
                foreach( $paths as $file ) {
                    if ( file_exists( $file ) ) {
                        include_once $file;
                    }
                }
            }
        }

        public static function setup() {

            $menus = array('primary' => esc_html__( 'Primary', PREFIX ));

            if (function_exists('get_field')) :

                switch (get_field('footer-layout', 'options')) :
                    case 'content-1-col':
                        $menus['footer'] = esc_html__( 'Footer', PREFIX );
                        break;
                    case 'content-3-cols':
                        $menus['footer-col-1'] = esc_html__( 'Footer 1 column', PREFIX );
                        $menus['footer-col-2'] = esc_html__( 'Footer 2 column', PREFIX );
                        $menus['footer-col-3'] = esc_html__( 'Footer 3 column', PREFIX );
                        break;
                endswitch;

            endif;

            // This theme uses wp_nav_menu() in one location.
            register_nav_menus($menus);

            add_image_size( '80px', 80, 80 );
            add_image_size( '120px', 120, 120 );
            add_image_size( '140px', 140, 140 );
            add_image_size( '170px', 170, 170 );
            add_image_size( '350px', 350, 350 );

            add_theme_support( 'title-tag' );

            // Add theme support for selective refresh for widgets.
            add_theme_support( 'customize-selective-refresh-widgets' );

        }

        public static function run_on_init() {

            if ( ! class_exists('\ACF') ) :

                Utility::show_notice(sprintf(
                    __('%s theme requires %sACF Pro%s plugin to be installed and active!'),
                    '<b>'.THEME_NAME.'</b>',
                    '<a href="https://www.advancedcustomfields.com/pro/" target="_blank">',
                    '</a>'
                ), 'error');

            endif;

            if ( ! defined( 'WP_BOOTSTRAP_BLOCKS_PLUGIN_FILE' ) ) :

                Utility::show_notice(sprintf(
                    __('%s theme requires %sBootstrap Blocks%s plugin to be installed and active!'),
                    '<b>'.THEME_NAME.'</b>',
                    '<b>',
                    '</b>'
                ), 'error');

            endif;

        }

        public static function get_the_archive_title() {

            $post_type_title = post_type_archive_title( '', false );

            if ( is_category() ) {
                $title = __( 'Category Archives: ', PREFIX ) . '<span class="page-description">' . single_term_title( '', false ) . '</span>';
            } elseif ( is_tag() ) {
                $title = __( 'Tag Archives: ', PREFIX ) . '<span class="page-description">' . single_term_title( '', false ) . '</span>';
            } elseif ( is_author() ) {
                $title = __( 'Author Archives: ', PREFIX ) . '<span class="page-description">' . get_the_author_meta( 'display_name' ) . '</span>';
            } elseif ( is_year() ) {
                $title = sprintf( esc_html__( 'Yearly %s for ', PREFIX ), $post_type_title) . '<span class="page-description">' . get_the_date( _x( 'Y', 'yearly archives date format', 'twentynineteen' ) ) . '</span>';
            } elseif ( is_month() ) {
                $title = sprintf( esc_html__( 'Monthly %s for ', PREFIX ), $post_type_title) . '<span class="page-description">' . get_the_date( _x( 'F Y', 'monthly archives date format', 'twentynineteen' ) ) . '</span>';
            } elseif ( is_day() ) {
                $title = sprintf( esc_html__( 'Daily %s for ', PREFIX ), $post_type_title) . '<span class="page-description">' . get_the_date() . '</span>';
            } elseif ( is_post_type_archive() ) {
                if (get_post_type() === 'release_notes') :
                    $title = sprintf( esc_html__( 'COOP: %s', PREFIX ), $post_type_title);
                else :
                    $title = $post_type_title;
                endif;
            } elseif ( is_tax() ) {
                $tax = get_taxonomy( get_queried_object()->taxonomy );
                /* translators: %s: Taxonomy singular name */
                $title = $tax->labels->singular_name ;
            } else {
                $title = __( 'Archives', PREFIX );
            }
            return $title;
        }


        public static function add_custom_image_sizes( $sizes ) {
            return array_merge( $sizes, array(
                '80px' => __('80x80px', PREFIX),
                '120px' => __('120x120px', PREFIX),
                '170px' => __('170x170px', PREFIX),
                '350px' => __('350x350px', PREFIX),
            ) );
        }


        public static function filter_archives_link( $link_html, $url, $text, $format ) {
            if ($format === 'html') :
                $current_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

                if ($current_url === $url) :
                    $link_html = str_replace('<a href=', '<a class="active" href=',  $link_html);
                endif;
            endif;

            return $link_html;
        }


        public static function add_mime_types($mime_types){
            $mime_types['svg']   = 'image/svg+xml';

            return $mime_types;
        }


        public static function replace_src_with_data_aload($attr) {
            if (!is_admin()) :
                $attr['data-aload'] = $attr['src'];
                $attr['src'] = 'data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==';

                if (isset($attr['srcset']) && $attr['srcset']) :
                    $attr['data-aload-srcset'] = $attr['srcset'];
                    $attr['srcset'] = 'data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==';
                endif;
            endif;

            return $attr;
        }

        /**
         * Add dropdown icon if menu item has children.
         *
         * @param  string  $title The menu item's title.
         * @param  \WP_Post $item  The current menu item.
         * @param  array   $args  An array of wp_nav_menu() arguments.
         * @param  int     $depth Depth of menu item. Used for padding.
         * @return string  $title The menu item's title with dropdown icon.
         */
        public static function set_app_images_nav_menu_item_title($title, $item, $args, $depth) {
            if ($depth === 0 && isset($item->classes) && is_array($item->classes)) :
                foreach ($item->classes as $class) :
                    $app_name = explode('menu_image_app_', $class);

                    if (is_array($app_name) && array_key_exists(1, $app_name)) :
                        $path = get_template_directory_uri() . '/assets/imgs/apps/' . $app_name[1] . '.svg';
                        $image = !is_admin() ? '<img data-aload="' . $path . '" alt="" width="177" height="60"
                            src="data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                            target="_blank" />'
                            : '<img src="' . $path . '" alt="" width="177" height="60" target="_blank" />';

                        return $image;
                    endif;
                endforeach;
            endif;

            return $title;
        }

    }

    Core::instance();

endif;