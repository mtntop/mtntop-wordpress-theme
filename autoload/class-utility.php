<?php
/**
 * This is a utility class, it contains useful methods
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class Utility {


    /**
     * Get content of a given file
     *
     * @since 1.0.0
     * @param string $file
     * @param mixed $vars
     * @return mixed
     */
    public static function get_tpl($file, $vars = array()){

        extract($vars);

        $c = '';

        if(file_exists(get_template_directory() . '/' . $file . '.php')){

            ob_start();

            include get_template_directory() . '/' . $file . '.php';

            $c = ob_get_clean();
        }

        return $c;
    }



    /**
     * Print array/obj in a readable format
     *
     * @since 1.0.0
     * @param array|object $data
     * @param boolean $exit
     * @return void
     */
    public static function pr($data, $exit = false){

        echo '<pre>'.print_r($data, 1).'</pre>';

        if($exit) exit;
    }

    /**
     * Log errors in a error.log file in the root of the plugin folder
     *
     * @since 1.0.0
     * @param mixed $msg
     * @param string $code
     * @return void
     */
    public static function error_log($msg, $code = ''){

        if(!is_string($msg)){
            $msg = print_r( $msg, true );
        }

        error_log('Error '.$code.' ['.date('Y-m-d h:m:i').']: '.$msg.PHP_EOL, 3, ERROR_PATH);
    }

    /**
     * @param array $pairs
     * @param array $atts
     * @return array
     */
    public static function atts($pairs = array(), $atts = array()) {
        $atts = (array)$atts;
        $out = array();

        foreach ($pairs as $name => $default) {
            if ( array_key_exists($name, $atts) )
                $out[$name] = $atts[$name];
            else
                $out[$name] = $default;
        }

        return $out;
    }

    /**
     * Display admin notice
     *
     * @since 1.0.0
     * @param string $msg
     * @param string $type
     * @return string
     */
    public static function show_notice($msg, $type = 'error'){
        add_action('admin_notices', function() use ($msg, $type){
            echo '<div class="notice notice-'.$type.'"><p>'.$msg.'</p></div>';
        });
    }
}