<?php
/**
 * Post types
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class FieldGroups {

    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function instance() {

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fields constructor.
     */
    function __construct () {

        // add_filter('acf/settings/show_admin', '__return_false');

        if ( function_exists('acf_add_local_field_group') ) :

            self::register_feature_card();

            self::register_hoa_websites();

            self::register_testimonials();

            self::register_testimonial();

            self::register_shared_components();

            self::register_overview();

            self::register_page();

        endif;

    }


    private static function register_page() {
        acf_add_local_field_group(array(
            'key' => 'group_5d24a9f424eaf',
            'title' => __('Shared components', PREFIX),
            'fields' => array(
                array(
                    'key' => 'field_5d24a9f8b4e3b',
                    'label' => __('Transparent Header', PREFIX),
                    'name' => 'header_transparent',
                    'type' => 'true_false',
                    'required' => 0,
                    'default_value' => 0,
                    'ui' => 1,
                ),
                array(
                    'key' => 'field_5d24a9f8b4e2a',
                    'label' => __('White Footer', PREFIX),
                    'name' => 'footer_white',
                    'type' => 'true_false',
                    'required' => 0,
                    'default_value' => 0,
                    'ui' => 1,
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'page',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));
    }


    private static function register_feature_card() {
        acf_add_local_field_group(array(
            'key' => 'group_5d18d4f4216fc',
            'title' => __('Featured Card', PREFIX),
            'fields' => array(
                array(
                    'key' => 'field_5d18d6159a286',
                    'label' => __('Style', PREFIX),
                    'name' => 'style',
                    'type' => 'select',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'choices' => array(
                        'style-1' => __('Style 1', PREFIX),
                        'style-2' => __('Style 2', PREFIX),
                    ),
                    'default_value' => array(
                        0 => 'style-1',
                    ),
                    'allow_null' => 0,
                    'multiple' => 0,
                    'return_format' => 'value',
                    'ajax' => 0,
                    'placeholder' => '',
                ),
                array(
                    'key' => 'field_5d18d521d8fb3',
                    'label' => __('Icon', PREFIX),
                    'name' => 'icon',
                    'type' => 'text',
                    'instructions' => 'Place base64 file or URL',
                    'required' => 0,
                    'placeholder' => 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2MC45NTEiIGhlaWdodD0iODAiIHZpZXdCb3g9IjAgMCA2MC45NTEgODAiPjxkZWZzPjxzdHlsZT4uYXtmaWxsOiNjNmE1NTY7fS5ie2ZpbGw6bm9uZTtzdHJva2U6I2ZmZjtzdHJva2UtbWl0ZXJsaW1pdDoxMDtzdHJva2Utd2lkdGg6MS4zNzRweDt9LmN7ZmlsbDojMzQzYTQwO30uZHtmaWxsOiNmZmY7fTwvc3R5bGU+PC9kZWZzPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0zOTEuMjI2IC0xNjQuNjU0KSI+PHJlY3QgY2xhc3M9ImEiIHdpZHRoPSI1OS4zNjkiIGhlaWdodD0iNzcuNzc3IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzOTEuMjI1IDE2Ni44NzcpIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMzk2LjIwOSAxNzguNjYpIj48bGluZSBjbGFzcz0iYiIgeDI9IjIwLjU3IiB0cmFuc2Zvcm09Im1hdHJpeCgwLjk5OSwgMC4wMzUsIC0wLjAzNSwgMC45OTksIDE0Ljk4OCwgMCkiLz48bGluZSBjbGFzcz0iYiIgeDI9IjQ1Ljk1NSIgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTksIDAuMDM1LCAtMC4wMzUsIDAuOTk5LCAyLjMwMywgNi4wMDYpIi8+PGxpbmUgY2xhc3M9ImIiIHgyPSI0NS45NTUiIHRyYW5zZm9ybT0ibWF0cml4KDEsIDAuMDE3LCAtMC4wMTcsIDEsIDMuMDY2LCAxMS44NDUpIi8+PGxpbmUgY2xhc3M9ImIiIHgyPSI0NS45NTUiIHRyYW5zZm9ybT0ibWF0cml4KDAuOTk5LCAwLjAzNSwgLTAuMDM1LCAwLjk5OSwgMi4zMDMsIDE2Ljg4MykiLz48bGluZSBjbGFzcz0iYiIgeDI9IjQ1Ljk1NSIgdHJhbnNmb3JtPSJtYXRyaXgoMSwgMC4wMTcsIC0wLjAxNywgMSwgMC43NDUsIDIyLjcyMykiLz48bGluZSBjbGFzcz0iYiIgeDI9IjQ1Ljk1NSIgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTksIDAuMDM1LCAtMC4wMzUsIDAuOTk5LCAyLjMwMywgMjcuNzYxKSIvPjxsaW5lIGNsYXNzPSJiIiB4Mj0iNDUuOTU1IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgzLjA2MiAzNC4wMDIpIi8+PGxpbmUgY2xhc3M9ImIiIHgyPSI0NS45NTUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDIuMjg5IDM5LjQ0MSkiLz48bGluZSBjbGFzcz0iYiIgeDI9IjQ1Ljk1NSIgdHJhbnNmb3JtPSJtYXRyaXgoMC45OTksIC0wLjAzNSwgMC4wMzUsIDAuOTk5LCAyLjMwMywgNDUuNjgyKSIvPjxsaW5lIGNsYXNzPSJiIiB4Mj0iNDUuOTU1IiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgwIDUxLjUyMSkgcm90YXRlKC0zKSIvPjxsaW5lIGNsYXNzPSJiIiB5MT0iMi4zMiIgeDI9IjQ1LjYzNiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMi4zNjggNTQuNDI2KSIvPjwvZz48Y2lyY2xlIGNsYXNzPSJjIiBjeD0iMTIuNzE3IiBjeT0iMTIuNzE3IiByPSIxMi43MTciIHRyYW5zZm9ybT0idHJhbnNsYXRlKDQxNi4yMDggMjIxLjY0OSkgcm90YXRlKC00NSkiLz48cGF0aCBjbGFzcz0iZCIgZD0iTTQzNy4xLDIzMS4zMzlsNi44NTcsNi42MTYsMTEuNjU0LTEzLjQ5LTIuMjgtMS45NzktOS4zNzQsMTAuNzU4LTQuNTM5LTQuMjMzWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTEyLjcxMiAtOC41MTYpIi8+PHBhdGggY2xhc3M9ImMiIGQ9Ik00NDAuNTU4LDE2NC42NTRINDA5LjE2NnY1LjZINDExLjJsLjQ1LjYxNC44MjQsMS4yNjUuMy40MzloMjQuNTkzbC4yOTItLjQzOS44MjMtMS4yNjUuNC0uNjE0aDIuMDgydi01LjZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNC4wNjQpIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNDEzLjAzOCAxNjUuNzI4KSI+PHJlY3QgY2xhc3M9ImQiIHdpZHRoPSIxNi4xMjkiIGhlaWdodD0iMy4xNTMiLz48L2c+PC9nPjwvc3ZnPg==',
                    'conditional_logic' => 0,
                ),
                array(
                    'key' => 'field_5d18d53ad8fb4',
                    'label' => __('Headline', PREFIX),
                    'name' => 'headline',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                ),
                array(
                    'key' => 'field_5d18d545d8fb5',
                    'label' => __('Description', PREFIX),
                    'name' => 'description',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '3',
                    'new_lines' => '',
                ),
                array(
                    'key' => 'field_5d18d5bed8fb6',
                    'label' => __('Reference', PREFIX),
                    'name' => 'reference',
                    'type' => 'group',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'layout' => 'block',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_5d18d5d2d8fb7',
                            'label' => __('Label', PREFIX),
                            'name' => 'label',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '50',
                            ),
                        ),
                        array(
                            'key' => 'field_5d18d5e5d8fb8',
                            'label' => __('Page', PREFIX),
                            'name' => 'page',
                            'type' => 'page_link',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '50',
                            ),
                            'post_type' => array(
                                0 => 'post',
                                1 => 'page',
                            ),
                            'taxonomy' => '',
                            'allow_null' => 1,
                            'allow_archives' => 1,
                            'multiple' => 0,
                        ),
                    ),
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'block',
                        'operator' => '==',
                        'value' => 'acf/feature-card',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));
    }


    private static function register_hoa_websites() {
        if (get_current_blog_id() === 6) :
            acf_add_local_field_group(array(
            'key' => 'group_5d18d4fsd4216fc',
            'title' => __('HOA Websites', PREFIX),
            'fields' => array(
                array(
                    'key' => 'field_5d2wq60e8f55b70',
                    'label' => 'Websites',
                    'name' => 'hoa-websites',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'layout' => 'block',
                    'button_label' => 'Add a website',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_5d260e9a55b71',
                            'label' => 'Cover image',
                            'name' => 'cover_image',
                            'type' => 'image',
                            'wrapper' => array(
                                'width' => '25',
                            ),
                            'return_format' => 'id',
                            'preview_size' => 'medium',
                            'library' => 'all',
                        ),
                        array(
                            'key' => 'field_5d260ebf55b72',
                            'label' => 'URL',
                            'name' => 'url',
                            'type' => 'text',
                            'wrapper' => array(
                                'width' => '75',
                            ),
                            'placeholder' => 'https://example.com',
                        ),
                    ),
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'hoa-websites',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));
        endif;
    }


    private static function register_testimonials() {
        acf_add_local_field_group(array(
            'key' => 'group_5d18d6d607374',
            'title' => __('Testimonials', PREFIX),
            'fields' => array(
                array(
                    'key' => 'field_5d18d6dbc8484',
                    'label' => __('Testimonials', PREFIX),
                    'name' => 'testimonials',
                    'type' => 'post_object',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'post_type' => array(
                        0 => 'testimonials',
                    ),
                    'taxonomy' => '',
                    'allow_null' => 1,
                    'multiple' => 1,
                    'return_format' => 'object',
                    'ui' => 1,
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'block',
                        'operator' => '==',
                        'value' => 'acf/testimonials',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));
    }


    private static function register_testimonial() {
        acf_add_local_field_group(array(
                'key' => 'group_5d18e7f82ff79',
                'title' => __('Testimonial', PREFIX),
                'fields' => array(
                    array(
                        'key' => 'field_5d18e7fed2a9c',
                        'label' => __('Jot title', PREFIX),
                        'name' => 'job_title',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'testimonials',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'acf_after_title',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));
    }


    private static function register_shared_components() {
        $id = 'shdc';

        acf_add_local_field_group(
            array (
                'key' => "$id-group",
                'title' => __('Shared components', PREFIX),
                'fields' => array (
                    array (
                        'key' => "$id-f200",
                        'label' => __('Header', PREFIX),
                        'name' => 'header-accordion',
                        'type' => 'accordion',
                    ),
                    array (
                        'key' => "$id-f210",
                        'label' => __('Logotype', PREFIX),
                        'name' => 'logotype',
                        'type' => 'image',
                        'return_format' => 'id',
                        'wrapper' => [
                            'width' => 70
                        ]
                    ),
                    array (
                        'key' => "$id-f222",
                        'label' => __('Logotype Size', PREFIX),
                        'name' => 'header-logo-size',
                        'type' => 'select',
                        'choices' => [
                            '80px' => __('Width 80px', PREFIX),
                            '120px' => __('Width 120px', PREFIX),
                            '140px' => __('Width 140px', PREFIX),
                            '170px' => __('Width 170px', PREFIX),
                            '350px' => __('Width 350px', PREFIX),
                        ],
                        'default_value' => '140px',
                        'wrapper' => [
                            'width' => 30
                        ]
                    ),
                    array (
                        'key' => "$id-f100",
                        'label' => __('Footer', PREFIX),
                        'name' => 'footer-accordion',
                        'type' => 'accordion',
                    ),
                    array(
                        'key' => "$id-f113",
                        'label' => __('Layout', PREFIX),
                        'name' => 'footer-layout',
                        'type' => 'button_group',
                        'choices' => array(
                            'content-3-cols' => 'Content col | 3 menu cols',
                            'content-1-col' => 'Content col | 1 menu col',
                        ),
                        'layout' => 'horizontal',
                        'return_format' => 'value',
                    ),
                    array (
                        'key' => "$id-f110",
                        'label' => __('Footer', PREFIX),
                        'name' => 'footer',
                        'type' => 'wysiwyg',
                    )
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'theme-shared',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
            )
        );
    }

    private static function register_overview() {
        $id = 'over';

        acf_add_local_field_group(
            array (
                'key' => "$id-group-overview",
                'title' => __('Overview', PREFIX),
                'fields' => array (
                    array (
                        'key' => "$id-o200",
                        'label' => __('Social Networks', PREFIX),
                        'name' => 'social-accordion',
                        'type' => 'accordion',
                    ),
                    array(
                        'key' => $id . '_5d2c80fb05499',
                        'label' => __('Social Networks', PREFIX),
                        'name' => 'social-networks',
                        'type' => 'flexible_content',
                        'layouts' => array(
                            $id . '_5d2c810fbc13d' => array(
                                'key' => $id . '_5d2c810fbc13d',
                                'name' => 'facebook',
                                'label' => 'Facebook',
                                'display' => 'block',
                                'sub_fields' => array(
                                    array(
                                        'key' => $id . '_5d2c81810549a',
                                        'label' => 'Url',
                                        'name' => 'url',
                                        'type' => 'text',
                                        'placeholder' => 'https://example.com',
                                    ),
                                ),
                                'max' => '1',
                            ),
                            $id . '_5d2c810fbc11d' => array(
                                'key' => $id . '_5d2c810fbc11d',
                                'name' => 'twitter',
                                'label' => 'Twitter',
                                'display' => 'block',
                                'sub_fields' => array(
                                    array(
                                        'key' => $id . '_5d2c81810544a',
                                        'label' => 'Url',
                                        'name' => 'url',
                                        'type' => 'text',
                                        'placeholder' => 'https://example.com',
                                    ),
                                ),
                                'max' => '1',
                            ),
                            $id . '_5d2c810fsc11d' => array(
                                'key' => $id . '_5d2c810fsc11d',
                                'name' => 'youtube',
                                'label' => 'YouTube',
                                'display' => 'block',
                                'sub_fields' => array(
                                    array(
                                        'key' => $id . '_5d2c812s1044a',
                                        'label' => 'Url',
                                        'name' => 'url',
                                        'type' => 'text',
                                        'placeholder' => 'https://example.com',
                                    ),
                                ),
                                'max' => '1',
                            ),
                            $id . '_5d2c810zsc11d' => array(
                                'key' => $id . '_5d2c810zsc11d',
                                'name' => 'googleplus',
                                'label' => 'Google+',
                                'display' => 'block',
                                'sub_fields' => array(
                                    array(
                                        'key' => $id . '_5d2c812sz044a',
                                        'label' => 'Url',
                                        'name' => 'url',
                                        'type' => 'text',
                                        'placeholder' => 'https://example.com',
                                    ),
                                ),
                                'max' => '1',
                            ),
                        ),
                        'button_label' => __('Add Network', PREFIX),
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'theme-shared',
                        ),
                    ),
                ),
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
            )
        );
    }

}