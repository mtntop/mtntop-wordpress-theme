<?php
/**
 * This is a utility class, it contains useful methods
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class ReleaseNotesArchivesWidget extends \WP_Widget {
    function __construct() {
        parent::__construct(
            'release-notes-archive',
            __('Release Notes Archive', PREFIX),
            ['description' => __('A monthly archive of your site’s Release Notes.', PREFIX)]
        );
    }

    // Вывод виджета
    public function widget( $args, $instance ){
        echo $args['before_widget'];

        echo '<h3 class="widgettitle">' . __('Updates', PREFIX) . '</h3>';

        echo '<ul>';

            wp_get_archives([ 'post_type' => 'release_notes' ]);

        echo '</ul>';

        echo $args['after_widget'];
    }
}