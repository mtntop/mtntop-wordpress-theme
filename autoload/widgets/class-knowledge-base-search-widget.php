<?php
/**
 * This is a utility class, it contains useful methods
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:59
 *
 * @since      1.0.0
 */

namespace CoopTheme;

class KnowledgeBaseSearchWidget extends \WP_Widget {
    function __construct() {
        parent::__construct(
            'knowledge-base-search',
            __('Knowledge Base Search', PREFIX),
            ['description' => __('A search box for searching within Knowledge Base.', PREFIX)]
        );
    }

    // Вывод виджета
    public function widget( $args, $instance ){
        echo $args['before_widget'];

        tpl('views/parts/shared/search', [
            'title' => __('More', PREFIX),
            'post_type' => 'knowledge-base',
        ]);

        echo $args['after_widget'];
    }
}