<?php
/**
 * Header template
 *
 * Created by PhpStorm.
 * User: apple
 * Date: 2019-06-29
 * Time: 17:49
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

    <?php tpl('views/parts/header'); ?>

    <div id="content" class="site-content">