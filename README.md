# The Coop Theme

Theme used for Coop.io and other websites of Mountain Top Consulting company.

## Installation

1. Set up Wordpress
1. Open `/wp-content/themes/` folder 
1. Clone the repository
    ```bash
    > git clone git clone https://{bitbucket_used_id}@bitbucket.org/mtntop/mtntop-wordpress-multisite-theme.git
    ```
    Notice `bitbucket_used_id` should be replaced with your bitbucket account id
1. Once the repository installed unfold it and set up javascript dependencies
    ```bash
    > npm install
    ```
    
## Development

### Assets bundling

**Run a watcher** then It will re-bundle the scripts and css files every time something is updated under `/src` folder
```bash
> npm run watch
```

**Before committing** you should run a command for preparing/optimizing assets for production usage
```bash
> npm run prod
```

### Important notices

* All files inside of `/autoload/` folder which have `class-` prefix will be automatically included
* Use `Object-oriented programming (OOP)` while develop the functionality
* Use namespace to encapsulate your PHP code
* Think modularly, extendable, and optimized as possible